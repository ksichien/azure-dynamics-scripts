#!/usr/bin/env pwsh
import-module 'C:\Program Files\Microsoft Dynamics NAV\100\Service\NavAdminTool.ps1'

$navinstances = @('nav100-live', 'nav100-dev')
$domain = 'vandelayindustries.com'
$users = @('avandelay', 'kvarnsen', 'ssadadmin')
$permissions = 'SUPER'

foreach ($user in $users) {
    foreach ($navinstance in $navinstanceances) {
        New-NAVServerUser -ServerInstance $navinstance -UserName $user -AuthenticationEmail "$user@$domain" -LicenseType Full -State Enabled
        New-NAVServerUserPermissionSet -PermissionSetId $permissions -ServerInstance $navinstance -UserName $user
    }
}
