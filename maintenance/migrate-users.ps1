#!/usr/bin/env pwsh
#Import-Module ActiveDirectory
#Import-Module 'C:\Program Files\Microsoft Dynamics NAV\100\Service\NavAdminTool.ps1'

$company = 'vandelayindustries'
$domain = 'dc=internal,dc=vandelayindustries,dc=com'
$email = 'vandelayindustries.com'
$server = 'dc1.internal.vandelayindustries.com'

$countries = @('china', 'germany', 'kenya', 'rwanda', 'tanzania')
$navinstances = @('nav100-live', 'nav100-dev')

foreach ($country in $countries) {
    $continent = switch ($country) {
        $countries[0] { 'asia' }
        $countries[1] { 'europe' }
        $countries[2] { 'africa' }
        $countries[3] { 'africa' }
        $countries[4] { 'africa' }
    }
    $ou = "ou=nav,ou=$country,ou=$continent,ou=users,ou=$company,$domain"
    $users = get-aduser -filter * -searchbase $ou -server $server
    
    foreach ($user in $users) {
        $aduser = get-aduser -identity $user -property * -server $server
        $mail = $aduser.mail -replace "@$email"
        
        $username = $aduser.samaccountname
        switch -wildcard ($aduser.distinguishedname) {
            '*china*' { $upn = $mail + '@' + "$company.net.cn" }
            '*germany*' { $upn = $mail + '@' + "$company.net" }
            '*kenya*' { $upn = $mail + '@' + "$company.co.ke" }
            '*rwanda*' { $upn = $mail + '@' + "$company.rw" }
            '*tanzania*' { $upn = $mail + '@' + "$company.co.tz" }
        }
        set-aduser -identity $aduser -userprincipalname $upn -server $server

        foreach ($navinstance in $navinstances) {
            set-navserveruser -serverinstance $navinstance -userName $username -authenticationemail $upn
        }
    }
}
