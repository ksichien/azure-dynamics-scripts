# Azure Dynamics Scripts
This is a small collection of PowerShell & SQL scripts I've written to automate the installation, configuration and maintenance of Microsoft Dynamics NAV.

## Overview

- database:
    - create-db-user
    - get-db-role-members
    - rename-db
- maintenance:
    - migrate-instances
    - migrate-users
- monitoring:
    - list-active-connections
- setup:
    - configure-aad-sync
    - configure-nav-auth
    - create-nav-instance
    - create-nav-user
    - install-nav-client
