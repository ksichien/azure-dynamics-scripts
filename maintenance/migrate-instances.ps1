#!/usr/bin/env pwsh
Import-Module 'C:\Program Files\Microsoft Dynamics NAV\100\Service\NavAdminTool.ps1'

$adtenant = 'vandelayindustries.com'
$dnsidentity = 'nav.vandelayindustries.com'
$navinstances = @('nav100-live', 'nav100-dev')

foreach ($navinstance in $navinstances) {
    $navserver = 'MicrosoftDynamicsNavServer$' + $navinstance

    $key = 'AppIdUri'
    $appiduri = switch($navinstance) {
        $navinstances[0] { "https://${adtenant}/XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX" }
        $navinstances[1] { "https://${adtenant}/YYYYYYYY-YYYY-YYYY-YYYY-YYYYYYYYYYYY" }
    }
    Set-NAVServerConfiguration -ServerInstance $navinstance -keyname $key -keyvalue $appiduri

    $key = 'WSFederationLoginEndpoint'
    $acsuri = "https://login.windows.net/${adtenant}/wsfed?wa=wsignin1.0%26wtrealm=${appiduri}"
    $wsfed = "${acsuri}%26wreply=https://${dnsidentity}/${navinstance}/WebClient/SignIn.aspx"
    Set-NAVServerConfiguration -ServerInstance $navinstance -keyname $key -keyvalue $wsfed

    $key = 'ClientServicesFederationMetadataLocation'
    $value = "https://login.windows.net/${adtenant}/FederationMetadata/2007-06/FederationMetadata.xml"
    Set-NAVServerConfiguration -ServerInstance $navinstance -keyname $key -keyvalue $value

    Set-NavServerInstance $navserver -restart
}
